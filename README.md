## Hall sensor emulator

This PCB is an extension board to a Festo CECC-LK PLC. The PLC provides a 28V peek-to-peek pwm signal and the hall sensor emulator should obtain a dual channel analog signal.
The analog signal should be between 17-10V and 5-12V respectively to the duty cycle of the input PWM signal. We can break down the tasks as:
* Capturing input signal PWM signal
* Generating outpwm signal and converting pwm to analog voltage
* Measuring output as a feedback

Capturing PWM signal could be performed by built in peripherals of most of MCUs. We should just convert the voltage to 3.3V level. Feedback is also straighforward 
by a voltage divider and an ADC. Generating analog output is a bit more interesting. First we should obtaing a pwm signal with a certain PWM and then smoothening the signal by an RC part.
Unfortunately if we keep adding voltage to the capactitor of the RC member but there is nothing connected to the output, there is nothing to take any current from the output, what means there will
be maximum voltage at the output. That's why we should use feedback and avoid driving the output when it is not needed. 

![alt text](Documentation/output_sch.PNG)

### PCB printscreens

![alt text](Documentation/v0.1_top_2D_pcb.PNG)

![alt text](Documentation/v0.1_top_3D_pcb.PNG)


